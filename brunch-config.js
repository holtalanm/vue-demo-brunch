module.exports = {
  files: {
    javascripts: {
      joinTo: 'app.js'
    },
    stylesheets: {
      joinTo: 'app.css'
    },
    templates: {
      joinTo: 'app.js'
    }
  },

  plugins: {
    babel: {
      presets: ['es2015']
    }
  },

  paths: {
    watched: [
      'src',
    ],

    public: 'static'
  },

  conventions: {
     assets: /^(src\/assets)/
  },

  npm: {
    globals: {
      $: 'jquery',
      jQuery: 'jquery',
      Vue: 'vue/dist/vue.js'
    }
  }
};