# vue-demo

> A quick demo of Vue.js utilizing Brunch and Vuex

## Build Setup

``` bash
#installs all npm dependencies
yarn
#build brunch
brunch build
#run brunch server
brunch watch --server
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
